# Progress.sh

#.sh
#It is a Bourne shell script. They are used in many variations of 
#UNIX-like operating systems. They have no "language" and are interpreted by 
#your shell (interpreter of terminal commands) or if the first line 
#is in the form

#Bourne shell script :is a text file intended to be executed as a set of 
    #commands for the Bourne shell, which is a shell (command line interpreter)
    #for Unix-style operating systems. 


# Build pylsl (can use pip2 also):

pip install pylsl

# PIP: pip is the preferred installer program 
#PYLSL:Python interface to the Lab Streaming Layer

#lab streaming layer (LSL) : provides a set of functions to make 
                            #instrument data accessible in real time within 
                            #a lab network
# To start OpenBCI_LSL
git clone https://github.com/OpenBCI/OpenBCI_LSL.git
cd OpenBCI_LSL/
sudo -H pip2 install -r requirements.txt 

#OpenBCI/OpenBCI_LSL:  This is the module that handles the creation and
                    #function of LSL using OpenBCI data."
                    #An application for establishing an OpenBCI stream through
                    #the Lab Streaming Layer (LSL)"
                    
#lab streaming layer (LSL) : provides a set of functions to make 
                            #instrument data accessible in real time within 
                            #a lab network.

#OpenBCI: Is an open source brain-computer interface platform,

# Find port to force open (otherwise it will try to find automagically) 
ls /dev/*
python openbci_lsl.py [YOUR PORT HERE] --stream

#/DEV: is the location of special or device files

# Receive data (to test you can set up a random stream $python SendDataAdvanced.py )
python ReceiveDataInChunks.py 

